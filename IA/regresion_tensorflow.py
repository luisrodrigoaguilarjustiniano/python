from __future__ import absolute_import, division, print_function
import tensorflow as tf
import numpy as np
rng = np.random
print("The parameters have been randomly defined")
learning_rate = 0.02
training_steps = 1000
display_step = 100

X = np.array([3.3,4.43,5.15,6.17,6.9,4.62,9.70,6.81,7.95,3.63,
7.02,11.0,5.33,7.87,5.55,9.77,7.1])
Y = np.array([1.5,2.86,2.1,3.4,1.89,1.478,3.78,2.3456,2.89,1.874,
3.82,3.55,2.0,2.04,2.52,3.94,7.3])
print("The data has been generated")
A = tf.Variable(rng.randn(), name="weight")
b = tf.Variable(rng.randn(), name="bias")

def linear_reg(x):
   return A * x + b
def mean_square_error(y_pred, y_true):
   return tf.reduce_mean(tf.square(y_pred - y_true))
   optimizer = tf.optimizers.SGD(learning_rate)

def run_optimization():
   with tf.GradientTape() as g:
      pred = linear_reg(X)
      loss = mean_square_error(pred, Y)

   gradients = g.gradient(loss, [A, b])

   optimizer.apply_gradients(zip(gradients, [A, b]))
print("The data is being trained")
for step in range(1, training_steps + 1):
   run_optimization()
   if step % display_step == 0:
      pred = linear_reg(X)
      loss = mean_square_error(pred, Y)
      print("step: %i, loss: %f, W: %f, b: %f" % (step, loss, W.numpy(), b.numpy()))
print("The visualization of original data and data fit to model")
import matplotlib.pyplot as plt
plt.plot(X, Y, 'ro', label='Original data')
plt.plot(X, np.array(W * X + b), label='Line fit to data')
plt.legend()
plt.show()