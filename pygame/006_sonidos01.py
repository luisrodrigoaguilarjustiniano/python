# SONIDO DE FONDO.
import pygame
pygame.init()
import os

pygame.mixer.init()# iniciamos el soniodo
sonido_fondo = pygame.mixer.Sound(os.path.dirname(__file__)+"\\sonido\\fondo02.mp3")# se cargar un sonido
pygame.mixer.Sound.play(sonido_fondo)# Reproduce el sonido al iniciar el programa. 
# si deseas que el sonido reproduzca en bucle agrega el -1 a lado del parametro sonido_fondo
# pygame.mixer.Sound.play(sonido_fondo,-1) #sonido repetitivo

ventana = pygame.display.set_mode((800, 600)) 
clock = pygame.time.Clock() 
fps = 140 # Frames por segundo
NEGRO = (0, 0, 0) 
BLANCO = (255, 255, 255)
AZUL = (0, 0, 200)
VERDE = (0, 255, 0)
COlOR1 = (200, 255, 100)

vida=50

ancho,alto=50,50
jugador = pygame.Rect((400, 300), (ancho,alto))

barraizq = pygame.Rect((0, 0), (25,600))
barrader = pygame.Rect((775, 0), (25,600))
cuadrado = pygame.Rect((100, 100), (25,25))

x,y=0,0
interruptor=True
numero=1

fuente = pygame.font.Font('freesansbold.ttf', 32)



def funcioninterruptor(interruptor):
    if interruptor==True: 
        return False
    else:
        return True

while True:    
    clock.tick(fps)    
    for evento in pygame.event.get():                         
        if evento.type == pygame.KEYDOWN: 
            # Control de salida con la tecla escape
            if evento.key == pygame.K_ESCAPE: 
                quit()                

    #Control de teclas cursoras    
    tecla = pygame.key.get_pressed()   
    if tecla[pygame.K_LEFT]:
        jugador.move_ip(-2, 0)
    if tecla[pygame.K_UP]:
        jugador.move_ip(0, -2)
    if tecla[pygame.K_RIGHT]:
        jugador.move_ip(2, 0)
    if tecla[pygame.K_DOWN]:
        jugador.move_ip(0, 2)
    
    if cuadrado.colliderect(jugador):        
        cuadrado.left=x
        cuadrado.top=y
        vida=vida-1
        
        interruptor=funcioninterruptor(interruptor)

    if cuadrado.colliderect(barraizq):        
        cuadrado.left=x
        cuadrado.top=y
        interruptor=funcioninterruptor(interruptor)

    if cuadrado.colliderect(barrader):        
        cuadrado.left=x
        cuadrado.top=y        
        interruptor=funcioninterruptor(interruptor)            
    
        


    if interruptor==True:
        numero=2
    else:
        numero=-2

    cuadrado.left=cuadrado.left+numero 

    x=cuadrado.left
    y=cuadrado.top
    
    ventana.fill(NEGRO)     

    
    pygame.draw.rect(ventana,BLANCO, cuadrado)
    pygame.draw.rect(ventana,BLANCO, barrader)
    pygame.draw.rect(ventana,BLANCO, barraizq)
    pygame.draw.rect(ventana,COlOR1, jugador)

    texto_vida = fuente.render('Vida= '+str(vida), True, VERDE, AZUL)
    ventana.blit(texto_vida,(0,0))  
     

    pygame.display.update()

