#   Proyecto creado por TecnoProfe
#    youtube: https://www.youtube.com/tecnoprofe
import pygame
import os

ANCHO = 800
ALTO = 600
DIMENSIONES=(ANCHO,ALTO)
NEGRO = (100, 185, 185)

pygame.init()
pygame.mixer.init()
ventana = pygame.display.set_mode(DIMENSIONES)
pygame.display.set_caption("Aviones")
clock = pygame.time.Clock()

class Nave(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave2.png")
        self.image.set_colorkey(NEGRO)
        self.rect = self.image.get_rect()
        self.rect.centerx = ANCHO // 2
        self.rect.bottom = ALTO - 10
        self.speed_x = 0

    def update(self):
        self.speed_x = 0
        keystate=pygame.key.get_pressed()                
        if keystate[pygame.K_LEFT]:            
            self.speed_x = -3
        if keystate[pygame.K_RIGHT]:            
            self.speed_x = 3           
        self.rect.x += self.speed_x        
        
        # Control de salida de los bordes izquierdo y derecho.
        if self.rect.right > ANCHO:
            self.rect.right = ANCHO
        if self.rect.left < 0:
            self.rect.left = 0

all_sprites = pygame.sprite.Group()


avion = Nave()
all_sprites.add(avion)


# Juego Bucle
while True:
    # velocidad sujerida del juego
    clock.tick(60)
    # obtiene eventos de entrada
    for event in pygame.event.get():
        # Verifica si presionó el boton cerrar. 
        if event.type == pygame.QUIT:
            quit()        

    # Actualiza los sprite
    all_sprites.update()

    #Renderiza fondo noegro
    ventana.fill(NEGRO)
    all_sprites.draw(ventana)
    # dibujando todo
    pygame.display.flip()