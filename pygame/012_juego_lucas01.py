import pygame
import os
pygame.init()

ventana = pygame.display.set_mode((800,600))

pygame.display.set_caption("Primer Juego")
lucas_derecha = [pygame.image.load(os.path.dirname(__file__)+"\\img\\lucas\\lucas01.png"),
                 pygame.image.load(os.path.dirname(__file__)+"\\img\\lucas\\lucas02.png"),
                 pygame.image.load(os.path.dirname(__file__)+"\\img\\lucas\\lucas03.png"),
                 pygame.image.load(os.path.dirname(__file__)+"\\img\\lucas\\lucas04.png")]

clock = pygame.time.Clock()

x = 300
y = 400
width = 64
height = 64
vel = 1
isJump = False
jumpCount = 10
left = False
right = False
contador_img = 0


def redrawGameWindow():
    global contador_img    

    if contador_img>=3:
        contador_img = 0

    if right:
        ventana.blit(lucas_derecha[contador_img], (x,y))
        contador_img +=1

    
    pygame.display.update()


#mainloop
jugar = True
while jugar:
    clock.tick(60)
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            jugar = False

    keys = pygame.key.get_pressed()
    
    if keys[pygame.K_RIGHT] and x < 800 - width - vel:
        x += vel
        right = True        

    ventana.fill((255,0,0)) 
    redrawGameWindow()
    
    

pygame.quit()