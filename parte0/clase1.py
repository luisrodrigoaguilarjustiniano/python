# Tipos de datos en Python
a1="5"      # La variable a1 es de tipo string
b1=4        # La variable b1 es un tipo Int
c1=5.6      # La variable c1 es un tipo Float
# Tipos de entrada y salida
if b1==3:
    print("hola")

b1=b1+3
print("Saludos",b1)