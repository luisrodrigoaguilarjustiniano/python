from tkinter import *
from matematica import Ecuacion2grado

ventana=Tk()
ventana.geometry("800x800")

eti1=Label(text="valor a: ")
eti1.grid(row="0",column="0")

txt1=Entry()
txt1.grid(row="0",column="1")

eti2=Label(text="valor b: ")
eti2.grid(row="1",column="0")

txt2=Entry()
txt2.grid(row="1",column="1")

eti3=Label(text="valor c: ")
eti3.grid(row="2",column="0")

txt3=Entry()
txt3.grid(row="2",column="1")

def calculo():
    a=int(txt1.get())
    b=int(txt2.get())
    c=int(txt3.get())
    ecu=Ecuacion2grado(a,b,c)
    ecu.solucion()

    etix1.config(text=ecu.x1) 
    etix2.config(text=ecu.x2) 

btn1=Button(text="Ecuacion 2 G",command=calculo)
btn1.grid(row="3",column="0")

etix1=Label(text="x1")
etix1.grid(row="4",column="0")

etix2=Label(text="x2")
etix2.grid(row="4",column="1")


ventana.mainloop()

