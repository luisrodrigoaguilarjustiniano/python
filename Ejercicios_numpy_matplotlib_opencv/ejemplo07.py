import numpy as np
import cv2
import os
imagen= cv2.imread(os.path.dirname(__file__)+'\\spiderman.jpg')
imagen=cv2.resize(imagen,(300,300))
array_imagen = np.array(imagen)
array_resultado = (255 - array_imagen)*7
imagen_resultado = np.array(array_resultado, dtype=np.uint8)
cv2.imshow('Imagen original', imagen)
cv2.imshow('Imagen resultante', imagen_resultado)
cv2.waitKey(0)
cv2.destroyAllWindows()

