#Declaración de una lista
lista=["Efrain","Roxana","Jaime","Jose Luis","Noemy","Alfredo"]
#imprime una lista en su totalidad
print(lista[:])

#imprime una lista desde la posición de indice 2 hasta el final
print(lista[2:])

#imprime una lista desde la posición de indice 0 hasta el indice 4
#pero excluye la posición 4
print(lista[:4])

#imprime una lista desde la posición de indice 2 hasta el indice 4
print(lista[2:4])