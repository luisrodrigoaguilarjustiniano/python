import pygame

class Animal:
    nombre=None     
    genero=None
    edad=None   
    color=None
    peso=None
    tipo=None
    __codigo=None

    def come(self):
        print(self.nombre," come")
    
    def duerme(self):
        print(self.nombre," duerme")

    def caza(self):
        print(self.nombre," caza")
        
class Leon(Animal):
    cabellera=None
    def __init__(self,nombre,genero,edad,color,tipo=None,peso=None,codigo=None):
        self.nombre=nombre
        self.genero=genero
        self.edad=edad
        self.color=color
        self.tipo=tipo
        self.__codigo=codigo

    def ruge(self):
        print(self.nombre," dice grrrrraaawwww")


pantalla = pygame.display.set_mode((720, 480))
clock = pygame.time.Clock()
FPS = 60 # Frames per second.

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

leonizq = pygame.image.load('carita.png')
leonder = pygame.image.load('carita.png')
imagex = 0 
imagey = 0
pantalla.blit(image, (imagex, imagey))

rect = pygame.Rect((0, 0), (32, 32))

while True:  
    pantalla.fill(WHITE)
    pygame.draw.polygon(pantalla, WHITE, ((146, 0), (291, 106), (236, 277), (56, 277), (0, 106)))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:           
            quit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT: 
                imagex-=5
                print("IZQUIERDA")
            elif event.key == pygame.K_RIGHT:                
                imagex=imagex+5
                print("DERECHA",imagex)
            elif event.key == pygame.K_DOWN:
                imagey+=5
                print("ABAJO")
            elif event.key == pygame.K_UP:
                imagey-=5
                print("ARRIBA")
            elif event.key == pygame.K_ESCAPE:
                quit()
                

    pantalla.blit(image, (imagex, imagey))    
    pygame.display.update()
    pygame.display.flip()

# leonel=Leon("Leonel","Macho",10,"Naranja")
# print("--------Leon ", leonel.nombre)
# leonel.__codigo="#45456"
# print("Mi codigo es",leonel.__codigo)
# leonel.ruge()
# leonel.come()
# leonel.duerme()


# felipe=Leon("Felipe","Macho",5,"Mostaza")
# print("--------Leon ", felipe.nombre)
# felipe.come()


# pablo=Leon("Pablo","Macho",15,"Amarillo")
# print("--------Leon ", pablo.nombre)
# pablo.caza()
# pablo.come()









class Perro:
    nombre=None          # por defecto public
    genero=None
    __edad=None          # 2 guiones bajos es privado
    color=None
    _peso=None           # 1 guion bajo es protegido

    def ladra(self):
        print("Gau gauu")
    
    def muerde(self):
        print("Muerde")
    
    def come(self, alimento):
        print("comiendo ",alimento)