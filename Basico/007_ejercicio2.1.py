#18. Escriba un programa para calcular el precio 
#del IVA (impuesto al valor agregado) que es del 13%, 
#de un producto con un valor introducido desde teclado,
#y su precio final. Ejemplo Precio del producto 100 bs,
# el porcentaje del IVA=13 bs
# y el precio del producto + IVA=113bs

iva=13/100    #es lo mismo que decir 13%
precio=int(input("Introduzca el precio del producto: "))
inpuesto=precio*iva
#print(inpuesto)
precio_final=precio+inpuesto
print("Precio con factura: ",precio_final)