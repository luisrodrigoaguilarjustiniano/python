from Arbol import Nodo


def busqueda_amplitud(e_inicial,e_final):
    exito=False
    nodos_frontera=[]
    nodos_visitados=[]
    # Creación del primer nodo inicial o un ojbeto de tipo Nodo
    NodoInicial=Nodo(e_inicial)
    nodos_frontera.append(NodoInicial)
    while len(nodos_frontera)!=0 and (not exito):
        #nodo es el Nodo padre inicialmente
        nodo=nodos_frontera.pop()
        nodos_visitados.append(nodo)

        if nodo.obtener_datos()==e_final:
            exito=True
            return nodo
        else:
            # auxiliar es una lista con el nodo padre
            #hijo izquierdo
            aux=nodo.obtener_datos()
            auxiliar_hijo=[aux[1],aux[0],aux[2],aux[3]]
            hijo_izquierdo=Nodo(auxiliar_hijo)

            if hijo_izquierdo.en_lista(nodos_visitados) and not hijo_izquierdo.en_lista(nodos_frontera):
                nodos_frontera.append(hijo_izquierdo)

            #hijo central
            aux=nodo.obtener_datos()
            auxiliar_hijo=[aux[0],aux[2],aux[1],aux[3]]
            hijo_central=Nodo(auxiliar_hijo)

            if hijo_central.en_lista(nodos_visitados) and not hijo_central.en_lista(nodos_frontera):
                nodos_frontera.append(hijo_central)

            #hijo derecho
            aux=nodo.obtener_datos()
            auxiliar_hijo=[aux[0],aux[1],aux[3],aux[2]]
            hijo_derecho=Nodo(auxiliar_hijo)
            
            if hijo_derecho.en_lista(nodos_visitados) and not hijo_derecho.en_lista(nodos_frontera):
                nodos_frontera.append(hijo_derecho)
            
            #asingar hijos a su padre
            nodo.asignar_hijos([hijo_izquierdo,hijo_central,hijo_derecho])


estado_inicial=[1,2,4,3]
estado_final=[1,2,3,4]
solucion=busqueda_amplitud(estado_inicial,estado_final)
print("La solución es: ",solucion)

