import random

def obtener_jugada_usuario():
    while True:
        print("elige una opcion:")
        print("1. piedra")
        print("2. papel")
        print("3. tijera")
        print("4. salir")
        opcion = input("ingresa el numero de tu eleccion: ")
        if opcion.isdigit() and 1 <= int(opcion) <=4:
            return int(opcion)
        print("opcion invalida. intentalo de nuevo:")

def obtener_jugada_computadora():
    return random.randint(1, 3)

def obtener_nombre_jugada(jugada):
    if jugada == 1:
        return "piedra"            
    elif jugada == 2:
        return "papel"
    elif jugada == 3:
        return "tijera"

def determinar_ganador(jugada_ususario, jugada_computadora):
    if jugada_ususario == jugada_computadora:
        return "empate"
    elif jugada_ususario == 1 and jugada_computadora == 3:
        return "ganaste"
    elif jugada_ususario == 2 and jugada_computadora == 1:
        return "ganaste"
    elif jugada_ususario == 3 and jugada_computadora == 2:
        return "ganaste"
    else:
        return "perdiste"

def main():
    print("¡bienvenido al juego piedra, papel o tijera")

    while True:
        jugada_usuario = obtener_jugada_usuario()
        if jugada_usuario == 4:
            print("gracias por jugar. ¡hasta pronto!")
            break
        jugada_computadora = obtener_jugada_computadora()

        print("elegiste:" + obtener_nombre_jugada(jugada_usuario))
        print("la computadora eligio:" + obtener_nombre_jugada(jugada_computadora))

        resultado = determinar_ganador(jugada_usuario, jugada_computadora)
        print(resultado)
main()