# Proyecto: Conquista Intergaláctica - Aliens vs Naves Espaciales
Un épico juego de estrategia y destreza, desarrollado con Python, donde te enfrentas a una invasión alienígena para salvar el universo.

![Employee data](image/funcion.gif)
## Descripción del Proyecto

"Conquista Intergaláctica" es un juego de Python dinámico y emocionante, diseñado para desafiar tu estrategia y habilidades motoras. Controla tu nave espacial y lucha contra hordas de alienígenas invasores. El destino del universo está en tus manos!

## Configuración y Ejecución del Proyecto

Para configurar y ejecutar el juego, sigue los siguientes pasos:

- Clona este repositorio en tu sistema local.
- Asegúrate de tener Python 3.8 instalado.
- Instala las librerías necesarias utilizando: ``` bash pip install pygame ```
- Ejecuta el archivo main.py para iniciar el juego: python main.py
## Librerías Utilizadas

Este proyecto utiliza las siguientes librerías Python:
- [pygame](https://www.pygame.org/)

Pygame es una biblioteca de Python de código abierto diseñada para facilitar la creación de videojuegos y otras aplicaciones multimedia. Pygame ofrece módulos para gestionar gráficos, sonido, eventos de entrada (como pulsaciones de teclas y clics del mouse) y otras funcionalidades comúnmente requeridas en el desarrollo de videojuegos.

## Libro: PROGRAMA Y LIBERA TU POTENCIAL
El libro titulado 'Programa y Libera tu Potencial' ha sido nuestra guía fundamental, proporcionándonos un sólido fundamento en la comprensión de los elementos esenciales de los algoritmos y la programación en Python. Su enfoque en la liberación del potencial creativo a través de la codificación ha sido un pilar esencial en la concepción y desarrollo de este proyecto.

![Employee data](image/book.jpg)

Sobre los autores:
(http://programatupotencial.com)
## Agradecimientos

Queremos agradecer a nuestro docente, al decano de la facultad de ingeniería y a la Universidad Privada Domingo Savio - Sede (Santa Cruz) por su apoyo y orientación a lo largo de este proyecto.
## Cómo Contribuir
Como proyecto universitario, las contribuciones están limitadas a los miembros del equipo. Sin embargo, si encuentras algún error o tienes alguna sugerencia para mejorar el código o los análisis, no dudes en contactarte con el docente o el equipo de desarrollo.

Para cualquier pregunta o comentario, por favor contacta al correo electrónico (sc.jaime.zambrana.c@upds.net.bo).

## Equipo de desarrollo
[UPDS](https://www.facebook.com/UPDS.bo)
Decano de la Facultad:
- [Msc. Wilmer Campos Saavedra](https://www.facebook.com/wilmercampos1)
Docente:
- [PhD.  JAIME ZAMBRANA CHACÓN](https://facebook.com/zambranachaconjaime)
Equipo de desarrollo:
- Nombre 1
- Nombre 2
- Nombre 3