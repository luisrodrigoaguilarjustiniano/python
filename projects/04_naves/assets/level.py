import pygame
import random
import sys
import math
from game import resource_pack, screen, screen_height, screen_width,level_font,playerimg, bulletimg, over_font, background, clock, asset_intro


# posicion inicial
playerX = 300
playerY = 500
playerX_change = 0

# lista de enemigos
enemyimg = []
enemyX = []
enemyY = []
enemyX_change = []
enemyY_change = []
no_of_enemies = 7

# posiciones de los enemigos
for i in range(no_of_enemies):
    enemy1 = resource_pack('assets/images/enemy1.png')
    enemyimg.append(pygame.image.load(enemy1))

    enemy2 = resource_pack('assets/images/enemy2.png')
    enemyimg.append(pygame.image.load(enemy2))

    # posicion aleatoria en X y Y del enemigo
    enemyX.append(random.randint(0, 500))
    enemyY.append(random.randint(-50, 200))

    # velocidad de movimiento del enemigo
    enemyX_change.append(5)
    enemyY_change.append(20)

# posicion de la bala
bulletX = 200
bulletY = 480
bulletY_change = 20
bullet_state = "ready"

score = 0

def show_score():
    score_value = level_font.render("SCORE " + str(score), True, (255, 255, 255))
    screen.blit(score_value, (10, 10))

# dibujar al jugadorGG
def player(x, y):
    screen.blit(playerimg, (x, y))

# dibujar al enemigo
def enemy(x, y, i):
    screen.blit(enemyimg[i], (x, y))

# dispara bala
def fire_bullet(x, y):
    global bullet_state
    bullet_state = "fire"
    screen.blit(bulletimg, (x + 16, y + 10))

# colision
def iscollision(enemyX, enemyY, bulletX, bulletY):
    distance = math.sqrt((math.pow(enemyX - bulletX, 2)) + (math.pow(enemyY - bulletY, 2)))
    if distance < 45:
        return True
    else:
        return False

# game over
def game_over_text():
    over_text = over_font.render("GAME OVER", True, (255, 255, 255))
    screen.blit(over_text, (screen_width/2 - over_text.get_width()/2, screen_height/2 - over_text.get_height()/2))

# funcion principal
def gameloop():
    global score
    global playerX
    global playerX_change
    global bulletX
    global bulletY
    global bullet_state

    in_game = True
    while in_game:

        # manejar eventos, actualizar y renderizar el juego
        screen.fill((0, 0, 0))
        screen.blit(background, (0, 0))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                in_game = False
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    playerX_change = -5
                elif event.key == pygame.K_RIGHT:
                    playerX_change = 5
                elif event.key == pygame.K_SPACE:
                    if bullet_state == "ready":
                        bulletX = playerX
                        fire_bullet(bulletX, bulletY)

            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    playerX_change = 0
                    
        # posicion del jugador actualizada
        playerX += playerX_change

        if playerX <= 0:
            playerX = 0
        elif playerX >= 736:
            playerX = 736

        # bucle de enemigos
        for i in range(no_of_enemies):
            if enemyY[i] > 440:
                for j in range(no_of_enemies):
                    enemyY[j] = 2000
                game_over_text()
                break

            enemyX[i] += enemyX_change[i]
            if enemyX[i] <= 0:
                enemyX_change[i] = 5
                enemyY[i] += enemyY_change[i]
            elif enemyX[i] >= 736:
                enemyX_change[i] = -5
                enemyY[i] += enemyY_change[i]

            # colision de enemigo
            collision = iscollision(enemyX[i], enemyY[i], bulletX, bulletY)
            if collision:
                bulletY = 450
                bullet_state = "ready"
                score += 1
                enemyX[i] = random.randint(0, 700)
                enemyY[i] = random.randint(0, 150)
                
            enemy(enemyX[i], enemyY[i], i)

        if bulletY <= 0:
            bulletY = 480
            bullet_state = "ready"
        if bullet_state == "fire":
            fire_bullet(bulletX, bulletY)
            bulletY -= bulletY_change

        player(playerX, playerY)
        show_score()

        pygame.display.update()
        clock.tick(60)

gameloop()